# hello.dotnet-vue

A simple project checking the developer experience of frontend/backend combination vue/aspnetmvc and Okta as a login provider.

This is mostly a copy of

- [Build a Simple CRUD App with ASP.NET Core and Vue](https://developer.okta.com/blog/2018/08/27/build-crud-app-vuejs-netcore)
- [oktadeveloper/okta-aspnetcore-vue-crud-example](https://github.com/oktadeveloper/okta-aspnetcore-vue-crud-example)

## Frontend

```console
cd Vue
yarn
yarn run dev
```

## Backend

```console
cd AspNetCore
dotnet build
dotnet run
```
