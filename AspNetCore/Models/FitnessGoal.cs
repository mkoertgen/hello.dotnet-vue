﻿using System;

namespace AspNetCore.Models
{
    public class FitnessGoal
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public decimal Value { get; set; }
        public DateTime DateTime { get; set; }
    }
}