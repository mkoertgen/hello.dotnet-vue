﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using AspNetCore.Models;
using Microsoft.EntityFrameworkCore;

namespace AspNetCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FitnessGoalsController : ControllerBase
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly DbSet<FitnessGoal> _dbSet;

        public FitnessGoalsController(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = _dbContext.FitnessGoals;
        }

        [HttpGet]
        public async Task<ActionResult<List<FitnessGoal>>> Get()
        {
            return await _dbSet.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<FitnessGoal>> Get(string id)
        {
            return await _dbSet.FindAsync(id);
        }

        [HttpPost]
        public async Task Post(FitnessGoal model)
        {
            await _dbSet.AddAsync(model);
            await _dbContext.SaveChangesAsync();
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put(string id, FitnessGoal model)
        {
            var exists = await _dbSet.AnyAsync(f => f.Id == id);
            if (!exists) return NotFound();
            _dbSet.Update(model);
            await _dbContext.SaveChangesAsync();
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            var entity = await _dbSet.FindAsync(id);
            _dbSet.Remove(entity);
            await _dbContext.SaveChangesAsync();
            return Ok();
        }
    }
}