﻿using System;
using System.Collections.Generic;
using System.Linq;
using AspNetCore.Models;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace AspNetCore.Controllers
{
    // ReSharper disable once InconsistentNaming
    public class FoodRecordsController_Should
    {
        public FoodRecordsController_Should()
        {
            var thisMorning = DateTime.Today + TimeSpan.FromHours(7);
            var expected = new List<FoodRecord>
            {
                new FoodRecord {DateTime = thisMorning, Id = "id1", Value = 60, Name = "1 Slice of Bread"},
                new FoodRecord {DateTime = thisMorning + TimeSpan.FromHours(6), Id = "id2", Value = 350, Name = "Lunch"}
            };

            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase("Add_writes_to_database")
                .Options;

            // create dbContext - this could be any derived class from DbContext
            _dbContext = new ApplicationDbContext(options);
            _dbContext.FoodRecords.AddRange(expected);
            _dbContext.SaveChanges();
        }

        private readonly ApplicationDbContext _dbContext;

        [Fact]
        public void Get()
        {
            var sut = new FoodRecordsController(_dbContext);

            var expected = _dbContext.FoodRecords.ToList();
            var actual = sut.Get().Result.Value;
            actual.Should().BeEquivalentTo(expected);

            var first = expected.First();
            var record = sut.Get(first.Id).Result.Value;
            record.Should().BeEquivalentTo(first);
        }
    }
}